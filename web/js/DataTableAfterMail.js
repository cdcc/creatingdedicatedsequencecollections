$(document).ready(function (){
var uri = window.location.pathname;
var pageName = uri.substring(uri.lastIndexOf("/") + 1);


if (pageName === "say-hello") {
    var s = DataTableData;
    $("#testing").DataTable({
        data: s
    });

    $('#testing').on('click', 'tbody tr', function() {
        var gene_id = $("td", this).eq(0).text();
        var description = $("td", this).eq(1).text();
    });

    $(function() {
        $("#dialog").dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });

        $("#testing").on('click', 'tbody tr', function() {
            var gene_id = $("td", this).eq(0).text();
            var description = $("td", this).eq(1).text();
            $('#dialog > #span1').text(gene_id);
            $('#dialog > #span2').text(description);
            $("#buttonGene_ID").val(gene_id);
            $("#dialog").dialog("open");
        });
    });
}

else if (pageName === "fetchResults.do") {
    var s = DataTableData;
        var table = $('#testing').DataTable({
            data: s,
            sDom: 'T<"clear">lfrtip',
            oTableTools: {
                sRowSelect: "multi",
                aButtons: ["select_all", "select_none"]
            }
            
        });
        
       $('#buttonSubmitForm').click(function () {
            var accession = $("td",".selected");
            var len = accession.length;
            var arrayKeys = [];
            for (i = 0; i<len; i+=5){
                arrayKeys.push(accession.eq(i).text());
            }
            $("#hiddenTextField").val(arrayKeys);
            
            
            
            
    } );
      
}; 
});
