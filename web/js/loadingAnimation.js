$(document).ready(function () {
    $('#contact').submit(function () {
        if (!$("td").hasClass("has-error")) {
            var angle = 0;
            var timer = setInterval(function () {
                angle += 3;
                $("#testingspinner").rotate(angle);
            }, 1);
            
    $('#resetButton').click(function(){
        clearInterval(timer);
        angle = 0;
        $("#testingspinner").rotate(angle);
    });
        }
    });
    
});

