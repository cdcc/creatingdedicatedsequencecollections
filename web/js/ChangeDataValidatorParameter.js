$(document).ready(function(){
    if($("#typeofSearchOptions").val() !== "ec_number"){
        $("#value_gene_name_EC_number").removeAttr("data-validation-regexp");
        $("#value_gene_name_EC_number").removeAttr("data-validation-error-msg");
        $("#value_gene_name_EC_number").attr("data-validation","alphanumeric");
        $("#value_gene_name_EC_number").attr("data-validation-allowing","-.,; ");
    }
  $("#typeofSearchOptions").change(function(){
    var s = $("#typeofSearchOptions").val();
      if(s === "ec_number"){
        $("#value_gene_name_EC_number").removeAttr("data-validation-allowing");
        $("#value_gene_name_EC_number").attr("data-validation","custom");
        $("#value_gene_name_EC_number").attr("data-validation-regexp","^[\\d.]+$");
        $("#value_gene_name_EC_number").attr("data-validation-error-msg","You want to search for an ec number, allowed characters: numbers and dots");
//        alert($("#value_gene_name_EC_number").attr("data-validation")+"\n"+$("#value_gene_name_EC_number").attr("data-validation-regexp")+"\n"+$("#value_gene_name_EC_number").attr("data-validation-error-msg")+"\n");

      }
      else{
        $("#value_gene_name_EC_number").removeAttr("data-validation-regexp");
        $("#value_gene_name_EC_number").removeAttr("data-validation-error-msg");
        $("#value_gene_name_EC_number").attr("data-validation","alphanumeric");
        $("#value_gene_name_EC_number").attr("data-validation-allowing","-.,; ");
//        alert($("#value_gene_name_EC_number").attr("value")+"\n"+$("#value_gene_name_EC_number").attr("data-validation")+"\n"+$("#value_gene_name_EC_number").attr("data-validation-allowing"));
      }
  });
});
