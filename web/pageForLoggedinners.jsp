<%--
    Document   : pageForLoggedinners
    Created on : Jan 18, 2015, 3:45:23 PM
    Author     : Richard Rubingh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>

        <c:choose>
            <c:when test="${empty sessionScope.user}">
                <meta http-equiv="refresh" content="0; url=${initParam.base_url}index.jsp" />
            </c:when>
            <c:otherwise>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>JSP Page</title>
            </head>
            <body>
                <h1>Hello, this page is only avalable for logged in users.</h1>
                <%--<jsp:include page="includes/session_open_form.jsp"/>--%>
            </body>
        </html>
    </c:otherwise>
</c:choose>






