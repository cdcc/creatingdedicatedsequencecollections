<%--
    Document   : index
    Created on : Nov 26, 2013, 5:02:44 PM
    Author     : Stephan
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="includes/htmlheader.jsp">
            <jsp:param name="title_append" value="getParameterFromMail" />
        </jsp:include>
    </head>
    <body>
        <div id="main">
            <header>
                <div id="logo">
                    <div id="logo_text">
                        <h1><a href="index.jsp">Creating Dedicated Sequence Collections</a></h1>
                        
                    </div>
                </div>
                <nav>
                    <jsp:include page="includes/navigationBar.jsp">
                        <jsp:param name="fetchResults" value="selected" />
                    </jsp:include>
                </nav>
            </header>
            <div id="site_content">
                <div id="content">
                    <c:choose>
                        <c:when test="${empty requestScope.geneKeys}">
                            <h1>Results</h1>
                            <div> In the data table below your results are listed.<br />
                            By selecting them and then clicking the "Download FASTA file" button, you can download the file.<br />
                            Note: You have to download it possibly two times. The first download might be empty but the second won't.<br /><br /></div>
                            <div id="demo"></div>
                            <form action="fetchResults.do" method="POST">     
                                <input type="hidden" value="" id="hiddenTextField" name="hiddenTextField"/>
                                <input type="submit" id="buttonSubmitForm" value="Download Fasta file"/>
                            </form>
                            <table id="testing" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>accession</th>
                                        <th>def</th>
                                        <th>gaps</th>
                                        <th>bscore</th>
                                        <th>eval</th>
                                    </tr>
                                </thead>

                                <tfoot>
                                    <tr>
                                        <th>accession</th>
                                        <th>def</th>
                                        <th>gaps</th>
                                        <th>bscore</th>
                                        <th>eval</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <script type="text/javascript">var DataTableData = ${requestScope.jsontest}</script>
                        </c:when>
                        <c:otherwise>
                            <h1>These are the selected gene id's</h1>
                            ${requestScope.geneKeys}
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <footer>
                <div>Copyright &copy; Stephan Boersma, Richard Rubing & Ronald Nieuwenhuis <span id='clockbox'></span></div>
            </footer>
        </div>
        <jsp:include page="includes/jslinks.jsp"/>
    </body>
</html>
