<%--
    Document   : index
    Created on : Nov 26, 2013, 5:02:44 PM
    Author     : Stephan
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="includes/htmlheader.jsp">
            <jsp:param name="title_append" value="ArgumentsTestDynamic" />
        </jsp:include>
    </head>
    <body>
        <div id="main">
            <header>
                <div id="logo">
                    <div id="logo_text">
                        <h1><a href="index.jsp">Creating Dedicated Sequence Collections</a></h1>
                    </div>
                </div>
                <nav>
                    <jsp:include page="includes/navigationBar.jsp">
                        <jsp:param name="ArgumentsTestDynamic" value="selected" />
                    </jsp:include>
                </nav>
            </header>
            <div id="site_content">
                <div id="content">
                    <c:choose>
                        <c:when test="${not empty param.gene_ID}">
                            you chose a gene with gene id: ${param.gene_ID}. It will be sent to out next script, blast.
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${empty requestScope.jsonGeneAndDescriptionOutput}">
                                    <c:choose>
                                        <c:when test="${not empty exception}">
                                            <jsp:include page="includes/ArgumentsTestDynamic/reEnterForm.jsp" />
                                        </c:when>
                                        <c:otherwise>
                                            <jsp:include page="includes/ArgumentsTestDynamic/emptyform.jsp" />                                            
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    <script type="text/javascript">var DataTableData = ${requestScope.jsonGeneAndDescriptionOutput}</script>
                                    <jsp:include page="includes/ArgumentsTestDynamic/correctFormValueGiven.jsp" />                                            
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <footer>
                <div>Copyright &copy; Stephan Boersma, Richard Rubingh & Ronald Nieuwenhuis <span id='clockbox'></span></div>
            </footer>
        </div>
        <jsp:include page="includes/jslinks.jsp"/>
    </body>
</html>
