<%-- 
    Document   : jslinks
    Created on : 25-dec-2014, 14:39:59
    Author     : Gebruiker
--%>

<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/google_rotate.js"></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/RunningClock.js"></script>
<script type="text/javascript" src="js/hideShow.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script src="js/dataTables.tableTools.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/DataTableAfterMail.js"></script>
<script src="js/ChangeDataValidatorParameter.js" type="text/javascript"></script>
<script src="js/loadingAnimation.js" type="text/javascript"></script>
<script src="js/submitDatatableValues.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.1.47/jquery.form-validator.min.js"></script>
<script>
    $.validate();
</script>