<%-- 
    Document   : htmlheader
    Created on : 25-dec-2014, 14:24:28
    Author     : Gebruiker
--%>

<base href="${initParam.base_url}">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${param.title_append}</title>
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
<link href="css/dataTables.tableTools.css" rel="stylesheet" type="text/css"/>
<!--<link href="css/dataTables.tableTools.min.css" rel="stylesheet" type="text/css"/>-->
<link rel="stylesheet" type="text/css" href="css/styleSheet.css" />