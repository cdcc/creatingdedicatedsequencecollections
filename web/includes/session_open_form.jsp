<%--
    Document   : session_open_form
    Created on : Jan 18, 2015, 4:23:37 PM
    Author     : zertox
--%>

<form name="login_form" action="servlet_login" method="POST">
    <br>
    <a href="/servlet_login"></a>
    <table border="1">
        <thead>
            <tr>
                <th>Welcome logged in user: ${sessionScope.user}
                </th>
            </tr>
        </thead>

        <tbody>

            <tr>
                <th>
                    <input type="submit" name="logout" value="logout"/>
                </th>
            </tr>
        </tbody>
    </table>
</form>