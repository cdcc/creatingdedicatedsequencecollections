<%-- 
    Document   : emptyform
    Created on : 25-dec-2014, 14:43:40
    Author     : Gebruiker
--%>

<h1>Input form</h1>
<p> Please fill in the following fields, including a valid email address, to search for a gene to submit.<br />
You can choose between a gene name and an E.C. number and both the input and desired output sequence.</p>
<jsp:include page="formTemplate.jsp" />