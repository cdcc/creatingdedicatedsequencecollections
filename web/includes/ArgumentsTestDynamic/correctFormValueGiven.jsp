<%-- 
    Document   : correctFormValueGiven
    Created on : 25-dec-2014, 14:46:19
    Author     : Gebruiker
--%>
<h1>Related genes to given input</h1>
<div id="dialog" title="Please confirm" style="display: none;">
    Are you sure this is the correct gene id: <br /><span id="span1">contains</span><br /><br />
    And/or the right description of the gene: <br /><span id="span2">contains</span><br />.
    <form action="say-hello" method="post">
        <button type=submit value="" name="gene_ID" id="buttonGene_ID">Ok</button> 
    </form>
</div>
<table id="testing" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Gene ID</th>
            <th>Description</th>
        </tr>
    </thead>

    <tfoot>
        <tr>
            <th>Gene ID</th>
            <th>Description</th>
        </tr>
    </tfoot>
</table>