<%-- 
    Document   : formTemplate
    Created on : 28-dec-2014, 15:04:29
    Author     : Gebruiker
--%>

<form id="contact" action="say-hello" method="post" name="contact-form">
    <div class="form_settings">  
    <table>
        <tr>
            <td>first name</td>
            <td><input id="first_name" class="contact" type="text" name="first_name" value="" data-validation="alphanumeric" data-validation-allowing='- '></td>
        </tr>
        <tr>
            <td>last name</td>
            <td><input class="contact" type="text" name="last_name" value="" data-validation="alphanumeric" data-validation-allowing=" -."/></td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td><input type="text" name="email" value="" data-validation="email"></td>
        </tr>
        <tr>
            <td>
                <select style="width: 100px" name="typeOfSearch" id="typeofSearchOptions">
                    <option value="ec_number">E.C. number</option>
                    <option value="gene_name">gene name</option>
                </select>
            </td>
            <!--<td><input id="value_gene_name_EC_number" class="contact" name="gene_name_value" value="2.7.7.4" data-validation="alphanumeric" data-validation-allowing=".-"/></td>-->
            <td><input id="value_gene_name_EC_number" class="contact" name="gene_name_value" value="2.7.7.4" data-validation="custom" data-validation-regexp="^[\d.]+$"  data-validation-error-msg="You want to search for an ec number, allowed characters: numbers and dots"/></td>
        </tr>
        <tr>
            <td>input type</td>
            <td>
                <select style="width: 16.5em" name="inputType">
                    <option value="AA">Amino Acid Sequence</option>
                    <option value="NT">Nucleotide Sequence</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>output type</td>
            <td>
                <select style="width: 16.5em" name="outputType">
                    <option value="AA">Amino Acid Sequence</option>
                    <option value="NT">Nucleotide Sequence</option>
                </select>
            </td>
        </tr>
    </table>
          
        <p><input class="submit" type="submit" name="contact_submitted" value="send"/>
            <input class="submit" type="reset" name="resetted" value="reset" id="resetButton"/></p>
        <img src="images/dna_strand_2_thumb.gif" id="testingspinner" alt="testingspinner"/>
    </div>
</form>
