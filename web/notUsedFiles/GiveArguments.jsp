<%-- 
    Document   : index
    Created on : Nov 26, 2013, 5:02:44 PM
    Author     : Stephan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <jsp:include page="includes/htmlheader.jsp">
            <jsp:param name="title_append" value="GiveArguments" />	
        </jsp:include>
    </head>
    <body>
        <div id="main">
            <header>
                <div id="logo">
                    <div id="logo_text">
                        <h1><a href="index.jsp">Creating Dedicated Sequence Collections</a></h1>
                        <h2>Website Template</h2>
                    </div>
                </div>
                <nav>
                    <ul class="sf-menu" id="nav">
                        <li><a href="index.jsp">Home</a></li>
                        <li class="selected"><a href="GiveArguments.jsp">Contact Us</a></li>
                        <li><a href="ArgumentsTestDynamic.jsp">TestingDynamic</a></li>
                        <li><a href="jqueryTest.jsp">test Jquery</a></li>
                    </ul>
                </nav>
            </header>
            <div id="site_content">
                <div id="content">
                    <h1>Contact</h1>
                    <p>Please give your first name, last name and e-mail address so we can contact you if neccessary</p>
                    <form id="contact" action="say-hello" method="post">
                        <div class="form_settings">
                            <p><span>first name</span><input class="contact" type="text" name="first_name" value="" /></p>
                            <p><span>last name</span><input class="contact" type="text" name="last_name" value="" /></p>
                            <p><span>gene_name_value</span><input class="contact" type="text" name="gene_name_value" value="" /></p>
                            <!--<p><span>Message</span><textarea class="contact textarea" rows="5" cols="50" name="your_message"></textarea></p>-->
                            <p><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="send"/></p>
                        </div>
                    </form>
                    <button type="button" onclick="hideAndShow()" id="displaytext">Show</button>
                    <h2 id="toggletext" style="display: none">Hello ${param.first_name} ${param.last_name}, the current date and time are:<br /><br />${requestScope.date}</h2>
                </div>
            </div>
            <footer>
                <p>Copyright &copy; Stephan Boersma</p>
            </footer>
        </div>
        <script type="text/javascript" src="js/hideShow.js"></script>
    </body>
</html>