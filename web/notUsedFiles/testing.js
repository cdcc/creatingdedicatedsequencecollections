var s = ObjectTesting;

$("#testing").DataTable({
    data:s
});

$('#testing').on( 'click', 'tbody tr', function () {
    var gene_id = $("td", this).eq(0).text();
    var description = $("td", this).eq(1).text();
} );

$(function() {
    $( "#dialog" ).dialog({
      autoOpen: false,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
 
    $( "#testing" ).on( 'click','tbody tr',function() {
        var gene_id = $("td", this).eq(0).text();
        var description = $("td", this).eq(1).text();
        $('#dialog > #span1').text(gene_id);
        $('#dialog > #span2').text(description);
        $("#buttonGene_ID").val(gene_id);
        $( "#dialog" ).dialog( "open" );
    });
  });


