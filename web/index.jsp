<%--
    Document   : index
    Created on : Nov 26, 2013, 5:02:44 PM
    Author     : Stephan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

    <head>
        <jsp:include page="includes/htmlheader.jsp">
            <jsp:param name="title_append" value="Home page" />
        </jsp:include>
    </head>

    <body>
        <div id="main">
            <header>
                <div id="logo">
                    <div id="logo_text">
                        <h1><a href="index.jsp">Creating Dedicated Sequence Collections</a></h1>
                        <h2>Website Template</h2>
                        ${sessionScope.user}
                    </div>
                </div>
                <nav>
                    <ul class="sf-menu" id="nav">
                        <li class="selected"><a href="index.jsp">Home</a></li>
                        <li><a href="ArgumentsTestDynamic.jsp">TestingDynamic</a></li>
                    </ul>
                </nav>
            </header>
            <div id="site_content">
                <div id="link_to_sessions_page">
                    <a href="pageForLoggedinners.jsp">Click me for sessions page</a>
                </div>
                <div id="login_logout_forum">
                    <c:choose>
                        <c:when test="${empty sessionScope.user}">
                            <jsp:include page="includes/login_form.jsp"/>
                        </c:when>
                        <c:otherwise>
                            <jsp:include page="includes/session_open_form.jsp"/>
                        </c:otherwise>
                    </c:choose>
                </div>


                <div id="content">
                    <h1>Welcome to our web application</h1>
                    <p> ${param.gene_ID}</p>
                    <h3>Template</h3>
                    <p>This is a website template</p>
                </div>
            </div>
            <footer>
                <p>Copyright &copy; Stephan Boersma, Richard Rubingh & Ronald Nieuwenhuis </p>
            </footer>
        </div>
        <p>&nbsp;</p>
    </body>
</html>