/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.bioinf.rnieuwenhuis.executeblast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.Float.parseFloat;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rnieuwenhuis
 */
public class BlastExecutor {
    private static String evalue;
    private static String wordSize;
    private static String fasta;
    private static String id;
    private static Path outputFilePath;
    private static String commandFormat = "ssh sboersma@idefix blastall -p %s -d %s -i %s -e %.20f -m 7 -a 4 -W %d -o %s";
    private static String type;
    private static String db;
    private static String command;
    /**
     * Constructor of BlastExecutor. Assigns values to the instance variables.
     * Executes createCommand() and createCommandlineTask().
     * @param input
     */
    //TODO BLAST db must be chosen and blast type must be chosen. if statements mus be added for this
    public BlastExecutor(HashMap<String, String> input) {
        try {
            //assign values to instance variables
            for(Entry<String, String> entry : input.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (key.equals("evalue")) {
                    evalue = value;
                }
                if (key.equals("wordSize")) {
                    wordSize = value;
                }
                if (key.equals("pathToFile")) {
                    fasta = value;
                }
                if (key.equals("ID")) {
                    id = value;
                }
                if (key.equals("Type")) {
                    type = value;
                }
            }
            if (type.equals("blastp")) {
                db = "/data/datasets/nr/nr";
                wordSize = "3";
            } else if (type.equals("blastn")) {
                db = "/data/datasets/nt/nt";
                wordSize = "15";
            } else if (type.equals("tblastn")) {
                db = "/data/datasets/nt/nt";
                wordSize = "15";
            } else {
                db = "/data/datasets/nr/nr";
                wordSize = "3";                
            }
            Path queryFile;
            queryFile = Paths.get(fasta);
            String output = "/commons/student/2014-2015/Thema10/DedicatedSequenceTool/" + id + ".blastoutput";
            outputFilePath = Paths.get(output);
        } catch (NullPointerException e) {
            //TODO find out the right way to handle this exception.
            System.out.println(e);
        }
    }
    /**
     * Executes a commandline task as specified in createCommand().
     * only uses instance variables.
     */
    public void createCommandlineTask() {
        try {
            System.out.println(command);
            Process p = Runtime.getRuntime().exec(command);
            /* For testing purposes the commands terminal output can be printed 
            to screen. uncomment piece of code. */
//            BufferedReader in = new BufferedReader(
//                                new InputStreamReader(p.getInputStream()));
//            String line = null;
//            while ((line = in.readLine()) != null) {
//                System.out.println(line);
//            }
        } catch (IOException ex) {
            Logger.getLogger(BlastExecutor.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
    }
    /**
     * Formats a string which should be used as a command. The formatted string
     * is the instance variable that consists of a blank blast command.
     */
    public void createCommand() {
        command = String.format(commandFormat, type, db, fasta,
                parseFloat(evalue), Integer.parseInt(wordSize),
                outputFilePath.toString());
        System.out.println("command" + command);
    }
}

