/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.rnieuwenhuis.controller;

import com.google.gson.Gson;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import nl.bioinf.hgsrubingh.keggapisimplefied.GetGeneEntriesFromECNumber;
import nl.bioinf.hgsrubingh.keggapisimplefied.GetGeneEntriesFromGeneName;
import nl.bioinf.hgsrubingh.sendEmailMutt.CreateBashMail;
import nl.bioinf.hgsrubingh.sendEmailMutt.SendEmailMuttCommandline;
import nl.bioinf.rnieuwenhuis.blastoutputparser.BlastOutputParser;
import nl.bioinf.rnieuwenhuis.executeblast.BlastExecutor;

/**
 *
 * @author hgsrubingh,rnieuwenhuis,sboersma
 */
public class Controller {
    private String filePath;

    public Controller() {

    }

    public static void getQuerySequence(HttpServletRequest request) {
        try {
            //this code bloack gets gene entry from ec number
            //validation checks and the ec needs to be longer then or equeal to
            //4 places
//            GetGeneEntriesFromECNumber geneEntries = new GetGeneEntriesFromECNumber();
//            geneEntries.getGeneEntriesFromKegg(request.getParameter("gene_name_value"));

            if (request.getParameter("typeOfSearch").equals("gene_name")) {
                GetGeneEntriesFromGeneName geneEntries = new GetGeneEntriesFromGeneName();
                geneEntries.getGeneEntriesFromKegg(request.getParameter("gene_name_value"));
                Gson gson = new Gson();
                Object json = gson.toJson(geneEntries.getGeneEntries());
                request.setAttribute("jsonGeneAndDescriptionOutput", json);
            }

            if (request.getParameter("typeOfSearch").equals("ec_number")) {
                GetGeneEntriesFromECNumber geneEntries = new GetGeneEntriesFromECNumber();
                geneEntries.getGeneEntriesFromKegg(request.getParameter("gene_name_value"));
                Gson gson = new Gson();
                Object json = gson.toJson(geneEntries.getGeneEntries());
                request.setAttribute("jsonGeneAndDescriptionOutput", json);
            }

        } catch (Exception e) {
            request.setAttribute("exception", "true");
        }

    }

    public static void executeBlast(HashMap<String, String> blastParameters, String email) {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                BlastExecutor blastExecutor = new BlastExecutor(blastParameters);
                blastExecutor.createCommand();
                blastExecutor.createCommandlineTask();
//                 sending email
                CreateBashMail a = new CreateBashMail();
                a.runMAil(blastParameters.get("ID"),blastParameters.get("Type"), email);
                SendEmailMuttCommandline b = new SendEmailMuttCommandline();
                try {
                    b.sendMail();
                } catch (IOException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
                BlastOutputParser BlastOutputParser = new BlastOutputParser();
                List<ArrayList<String>> info = BlastOutputParser.parseOutputFile(blastParameters.get("ID"));
                Object json = BlastOutputParser.createReturnable(info);              
            }
        };
       Thread t = new Thread(r);
       t.start();
    }

    public Object parseBlast(String path) {
        BlastOutputParser BlastOutputParser = new BlastOutputParser();
        //BlastOutputParser.scpFileToLocalMachine(9384483);
        List<ArrayList<String>> info = BlastOutputParser.parseOutputFile(path);
        Object json = BlastOutputParser.createReturnable(info);
        //BlastOutputParser.deleteFile(9384483);
        return json;
    }

    public String createDownloadable(String[] accessions, String id, String dbType) throws IOException {
        FileWriter fw = null;
            String filePathForAccessions = "/commons/student/2014-2015/Thema10/DedicatedSequenceTool/"+id+".txt";
            String createdFilePath = "/commons/student/2014-2015/Thema10/DedicatedSequenceTool/"+id+".fasta";
            String[] accessionList = accessions[0].split(",");
            fw = new FileWriter(filePathForAccessions);
            BufferedWriter bw = new BufferedWriter(fw);
            for(String acc: accessionList){
                    bw.write(acc + "\n");
            }
            bw.close();
        String formatString = "blastdbcmd -db %s -dbtype %s -entry_batch %s -out /commons/student/2014-2015/Thema10/DedicatedSequenceTool/%s.fasta -target_only";
        // nr db is prot. nt db is nucl
        String command = null;
        if (dbType.equals("blastp") || dbType.equals("blastx")){
            command = String.format(formatString, "/data/datasets/nr/nr", "prot", filePathForAccessions, id);            
        } else if(dbType.equals("blastn") || dbType.equals("tblastn")){
            command = String.format(formatString, "/data/datasets/nt/nt", "nucl", filePathForAccessions, id);
        }
        try {
            Process p = Runtime.getRuntime().exec(command);
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return createdFilePath;
}
}
