/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.bioinf.rnieuwenhuis.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sboersma
 */
public class IdCreator {
    
    public void idCreator (){
    }
    
    public Integer readIdFile() {
        File file = new File("/commons/student/2014-2015/Thema10/DedicatedSequenceTool/idFile.important");
        String filePath = "/commons/student/2014-2015/Thema10/DedicatedSequenceTool/idFile.important";
        Path path = Paths.get(filePath);
        Integer preId = null;
        Integer id = null;
        try {
            Scanner scan = new Scanner(path , "utf-8");
                preId = Integer.parseInt(scan.next()); 
                id = preId + 1;
                FileWriter fw = new FileWriter(filePath);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(id.toString());
                bw.close();
        } catch (IOException ex) {
            Logger.getLogger(IdCreator.class.getName()).log(Level.SEVERE, null, ex);
        }
    return id;
    }
}
