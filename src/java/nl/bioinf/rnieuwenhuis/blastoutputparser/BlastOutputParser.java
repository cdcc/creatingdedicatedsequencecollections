/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package nl.bioinf.rnieuwenhuis.blastoutputparser;

import com.google.gson.Gson;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.cellingo.sequence_tools.blast.BlastHit;
import net.cellingo.sequence_tools.blast.BlastHsp;
import net.cellingo.sequence_tools.blast.BlastParser;
import net.cellingo.sequence_tools.blast.BlastQuery;
import net.cellingo.sequence_tools.blast.BlastResults;
import net.cellingo.sequence_tools.blast.HspProperty;
import net.cellingo.sequence_tools.blast.NcbiBlastParser;

/**
 * @author rnieuwenhuis
 */
public class BlastOutputParser {
   
    public BlastOutputParser() {
        
    }
    /**
     * 
     * @param id 
     */
    public void scpFileToLocalMachine(String id) {
        try {
            String fileToCopyFormat = "sboersma@idefix:/commons/student/2014-2015/Thema10/DedicatedSequenceTool/%s.blastoutput";
            String fileToCopy = String.format(fileToCopyFormat, id.toString());
            String[] scpCommand;
            scpCommand = new String[]{
                fileToCopy,
                "/commons/student/2014-2015/Thema10/DedicatedSequenceTool/"
            };
            ScpFrom.main(scpCommand);
        } catch (Exception e) {
        }
    }
    /**
     * 
     * @param id 
     */
    public List<ArrayList<String>> parseOutputFile(String path) {
        try {
            String fileStringFormat = "/commons/student/2014-2015/Thema10/DedicatedSequenceTool/%s.blastoutput";
            String fileString = path; //String.format(fileStringFormat, id.toString());
            BlastParser parser = new NcbiBlastParser();
            File f = new File(fileString);
		parser.setFile(f);
		
//		parser.parseStreaming(true, this);
		
		parser.parse();
		BlastResults results = parser.getBlastResults();
		Iterator<BlastQuery> queries = results.getQueriesWithHits();
                List<ArrayList<String>> hitList = new ArrayList<>();
		while( queries.hasNext() ){
			BlastQuery query = queries.next();
			Iterator<BlastHit> hits = query.getBlastHits();
			while( hits.hasNext() ){
				BlastHit hit = hits.next();
                                try {
                                    ArrayList<String> properties = new ArrayList<>();
                                    BlastHsp hsp = hit.getBestHsp();
                                    Double bscore = hsp.getPropertyValue(HspProperty.HSP_BIT_SCORE);
                                    Double gaps = hsp.getPropertyValue(HspProperty.HSP_GAP_OPENINGS);
                                    Double eval = hsp.getPropertyValue(HspProperty.HSP_EVALUE);
                                    String def = hit.getHitDefinition();
                                    String accession = hit.getHitAccession();
                                    properties.add(accession);
                                    properties.add(def);
                                    properties.add(gaps.toString());
                                    properties.add(bscore.toString());
                                    properties.add(eval.toString());
                                    hitList.add(properties);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
            return hitList;            
        } catch (Exception e) {
        }
        return null;
    }
    /**
     * 
     * @param info
     * @return 
     */
    public Object createReturnable(List<ArrayList<String>> info) {
        Gson gson = new Gson();
        Object json = gson.toJson(info);
        return json;
    }
    /**
     * 
     * @param id 
     */
    public void deleteFile(Integer id) {
        String fileStringFormat = "/homes/rnieuwenhuis/tmp/DedicatedSequenceTool/%s.blastoutput";
        String fileToDelete = String.format(fileStringFormat, id.toString());
        File file = new File(fileToDelete);
        try{
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
       
    }
}