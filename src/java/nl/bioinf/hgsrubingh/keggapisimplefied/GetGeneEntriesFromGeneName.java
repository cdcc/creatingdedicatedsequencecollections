/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.hgsrubingh.keggapisimplefied;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Richard Rubingh
 */
public class GetGeneEntriesFromGeneName {

    static private final String baseurl = "http://rest.kegg.jp/find/genes/";
    List<ArrayList<String>> geneEntries = new ArrayList<>();

    public void getGeneEntriesFromKegg(String geneName) throws MalformedURLException, IOException {

        String pattern = "[\\w\\d ]+";
        // Create the object pattern
        Pattern r = Pattern.compile(pattern);

        // Create matcher object
        Matcher m = r.matcher(geneName);

        //check the input for ec number.
        if (m.find()) {
            String filteredECNumber = m.group(0);
            //minimum size of a word is 4 places.
            if (4 >= filteredECNumber.length() == false) {

                URL urlRead = new URL(baseurl + geneName);

                try (BufferedReader input = new BufferedReader(
                        new InputStreamReader(urlRead.openStream()))) {
                    String inputLine;
                    while ((inputLine = input.readLine()) != null) {
                        ArrayList<String> geneEntryAndDef = new ArrayList<>();

                        String[] split = inputLine.split("\t");
                        String geneEntry = split[0];
                        String geneDef = split[1];

                        geneEntryAndDef.add(geneEntry);
                        geneEntryAndDef.add(geneDef);

                        geneEntries.add(geneEntryAndDef);

                    }

                }

            }

        }

    }

    @Override
    public String toString() {
        return "GetGeneEntriesFromGeneName{" + "geneEntries=" + geneEntries + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.geneEntries);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GetGeneEntriesFromGeneName other = (GetGeneEntriesFromGeneName) obj;
        if (!Objects.equals(this.geneEntries, other.geneEntries)) {
            return false;
        }
        return true;
    }

    public List<ArrayList<String>> getGeneEntries() {
        return geneEntries;
    }
}
