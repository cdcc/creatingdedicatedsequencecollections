/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.hgsrubingh.keggapisimplefied;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Richard Rubingh
 */
public class GetGeneEntrySequence {

    static private final String baseurl = "http://rest.kegg.jp/get/";
    String fasta;

    public void getGeneEntriesFromKegg(String geneEntry, String isAmino) throws MalformedURLException, IOException {
        StringBuilder fastaBuilder = new StringBuilder();

        String sequenceType;
        if ("AA".equals(isAmino)) {

            sequenceType = "/aaseq";

        } else {

            sequenceType = "/ntseq";

        }

        URL urlRead = new URL(baseurl + geneEntry + sequenceType);

        try (BufferedReader input = new BufferedReader(
                new InputStreamReader(urlRead.openStream()))) {
            String inputLine;
            while ((inputLine = input.readLine()) != null) {

                fastaBuilder.append(inputLine + "\n");

            }
            fasta = fastaBuilder.toString();

        }

    }

    public String getFasta() {

        return fasta;
    }

}
