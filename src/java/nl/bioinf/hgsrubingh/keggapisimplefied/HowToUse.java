///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package nl.bioinf.hgsrubingh.keggapisimplefied;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// *
// * @author Date: 27-11-2014 This program gets data from the rest kegg API. This
// * is parsed and saved as objects
// *
// */
//public class HowToUse {
//
//    static ArrayList<String> keggGeneEntries = new ArrayList<>();
//
//    /**
//     *
//     * @param args
//     * @throws java.lang.Exception
//     */
//    public static void main(String[] args) throws Exception {
//
//        //get gene names
//        //http://rest.kegg.jp/find/genes/sulfide
//        //get seq from gene entry
//        //http://rest.kegg.jp/get/hsa:10130/aaseq
//        //get genes by ec number
//        //http://rest.kegg.jp/find/genes/1.8.4.2
//        //starts program
//        /**
//         * The following part is where the file is being read.
//         *
//         * @ Author Richard Rubingh.
//         * @param args
//         */
//        // This code covers the get gene and entry from gene name
////        try {
////            GetGeneEntriesFromGeneName geneEntries = new GetGeneEntriesFromGeneName();
////            geneEntries.getGeneEntriesFromKegg("sulfide");
////            //validation checks and the word needs to be longer then or equeal to
////            //4 places
////            // get and put the unique ec numbers in an array.
////            for (List i : geneEntries.getGeneEntries()) {
////                System.out.println(i);
////            }
////
////        } catch (Exception e) {
////            System.out.println("Sorry there was en error processing the website information.");
////        }
////        try {
////            //this code bloack gets gene entry from ec number
////            //validation checks and the ec needs to be longer then or equeal to
////            //4 places
////            GetGeneEntriesFromECNumber geneEntries = new GetGeneEntriesFromECNumber();
////            geneEntries.getGeneEntriesFromKegg("1.8.4.2");
////            for (List i : geneEntries.getGeneEntries()) {
////
////                System.out.println(i);
////            }
////
////        } catch (Exception e) {
////            System.out.println("Sorry there was en error processing the website information.");
////
////        }
//        try {
//            //no checks yet
//            GetGeneEntrySequence g = new GetGeneEntrySequence();
//            // get and put the unique ec numbers in an array.
//            g.getGeneEntriesFromKegg("bay:RBAM_021290", true);
//
//            System.out.println(g.getFasta());
//
//        } catch (Exception e) {
//            System.out.println("Sorry there was en error processing the website information.");
//
//        }
////        try {
////            //no checks yet
////            GetOrganismFromGeneName g = new GetOrganismFromGeneName();
////            // get and put the unique ec numbers in an array.
////
////            g.getGeneEntriesFromKegg("aaa");
////
////            System.out.println(g.geneEntries);
////
////            g.getGeneEntriesFromKegg("ptg");
////
////            System.out.println(g.geneEntries);
////
////            g.getGeneEntriesFromKegg("hsa");
////
////            System.out.println(g.geneEntries);
////
////        } catch (Exception e) {
////            System.out.println("Sorry there was en error processing the website information.");
////
////        }
//    }
//
//}
