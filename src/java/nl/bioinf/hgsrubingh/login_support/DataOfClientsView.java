/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.hgsrubingh.login_support;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hgsrubingh
 */
public class DataOfClientsView {

    public static void main(String[] args) throws IOException {

        System.out.println(getDownloadListForUser("danny"));
    }

    public static ArrayList<String> getDownloadListForUser(String user) throws IOException {

        @SuppressWarnings("UseOfObsoleteCollectionType")

        ArrayList<String> wholeDatasetList = new ArrayList();

        File f = new File("/commons/student/2014-2015/Thema10/DedicatedSequenceTool/download_for_users/" + user + "/"); // user dir

        File[] files = f.listFiles();
        for (File file : files) {
            if (file.isDirectory() == false) {

                wholeDatasetList.add(file.getCanonicalPath());
            }
        }
        return wholeDatasetList;
    }
}
