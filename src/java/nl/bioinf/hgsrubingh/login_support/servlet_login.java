package nl.bioinf.hgsrubingh.login_support;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Richard Rubingh
 */
public class servlet_login extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/force-download");
        String userNameForm = request.getParameter("form_user_name");
        String userPasswordForm = request.getParameter("form_password");
        String logout = request.getParameter("logout");

        /*logout if  requested*/
        if (logout != null && logout.equals("logout")) {
            request.getSession().invalidate();
            RequestDispatcher view = request.getRequestDispatcher("index.jsp");
            view.forward(request, response);
        }

        if (userNameForm == null
                || userNameForm.length() == 0
                || userPasswordForm == null
                || userPasswordForm.length() == 0) {
            RequestDispatcher view = request.getRequestDispatcher("index.jsp");
            view.forward(request, response);

        } else {

            if (request.getParameter("register") != null) {

                try {
                    MysqlInteraction.registerUser(userNameForm, userPasswordForm);
                    RequestDispatcher view = request.getRequestDispatcher("index.jsp");
                    view.forward(request, response);

                } catch (ClassNotFoundException e) {
                } catch (ServletException e) {
                } catch (IOException e) {
                }
            }

            try {

                if (MysqlInteraction.validateUser(userNameForm, userPasswordForm)) {


                    /*create a session*/
                    HttpSession session = request.getSession();
                    session.setMaxInactiveInterval(30);
                    if (session.getAttribute("user") == null) {
                        session.setAttribute("user", userNameForm);
                    }

                    request.setAttribute("user", userNameForm);

                    try {

                        session.setAttribute("downloadList", listClientDownloadables.getDownloadListForUser((String) session.getAttribute("user")));
                    } catch (Exception e) {
                    }

                    RequestDispatcher view = request.getRequestDispatcher("index.jsp");
                    view.forward(request, response);
                } else {

                    RequestDispatcher view = request.getRequestDispatcher("index.jsp");
                    view.forward(request, response);
                }

            } catch (ClassNotFoundException e) {
            } catch (ServletException e) {
            } catch (IOException e) {
            }

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
