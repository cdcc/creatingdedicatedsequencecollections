package nl.bioinf.hgsrubingh.login_support;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Richard Rubingh
 */
public class MysqlInteraction {

    public static void main(String[] args) throws ClassNotFoundException {

        MysqlInteraction.registerUser("a", "a");

        System.out.println(MysqlInteraction.validateUser("a", "a"));

    }

    public static boolean validateUser(String formUserName, String formPassword) throws ClassNotFoundException {

        boolean login_validated = false;

        String mysqlDriver = "org.gjt.mm.mysql.Driver";
        Class.forName(mysqlDriver);

        Connection con = null;
        Statement st = null;
        ResultSet rs = null;

        String url = "jdbc:mysql://mysql.bin/Hgsrubingh";
        String user = "hgsrubingh";
        String password = "t66k6zu6";

        try {
            con = DriverManager.getConnection(url, user, password);
            st = con.createStatement();

            String sqlQuery = String.format("SELECT * FROM users WHERE username='%s' AND password='%s';", formUserName, formPassword);
            rs = st.executeQuery(sqlQuery);
            if (rs.next()) {
                if (rs.getString(1).length() != 0) {
                    login_validated = true;

                }
            }

        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(MysqlInteraction.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } finally {
            try {
                if (rs != null) {

                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }

            } catch (SQLException ex) {
                Logger lgr = Logger.getLogger(MysqlInteraction.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
        return login_validated;
    }

    static public void registerUser(String formUserName, String formPassword) throws ClassNotFoundException {

        try {
            // create a mysql database connection
            String myDriver = "org.gjt.mm.mysql.Driver";
            String myUrl = "jdbc:mysql://mysql.bin/Hgsrubingh";
            Class.forName(myDriver);
            Connection con = DriverManager.getConnection(myUrl, "hgsrubingh", "t66k6zu6");

            // the mysql insert statement
            String sqlQuery = " insert into users (username, password)"
                    + " values (?, ?)";

            // create the mysql insert preparedstatement
            PreparedStatement preparedStmt = con.prepareStatement(sqlQuery);
            preparedStmt.setString(1, formUserName);
            preparedStmt.setString(2, formPassword);

            // execute the preparedstatement
            preparedStmt.execute();

            con.close();
        } catch (ClassNotFoundException e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        } catch (SQLException e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }

    }
}
