/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.hgsrubingh.sendEmailMutt;

/**
 *
 * @author hgsrubingh
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CreateBashMail {

    public void runMAil(String id, String dbType, String email) {
        try {

            String filePath = "/commons/student/2014-2015/Thema10/DedicatedSequenceTool/netbeanswrode.sh";
            String executeProgram = "#!/bin/sh\n";
            String messageFormatString = "http://localhost:8080/DSCT/fetchResults.do?filePath=/commons/student/2014-2015/Thema10/DedicatedSequenceTool/%s.blastoutput\\&ID=%s\\&dbType=%s";
            String message = String.format(messageFormatString, id,id,dbType);
            String subject = "link to your blast results";

            String command = executeProgram + "echo " + message + " | mutt -s " + subject + " "+email;

            File file = new File(filePath);

            // if file doesnt exists, then create it
//            if (!file.exists()) {
                file.createNewFile();
//            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(command);
            bw.close();
            Process p = Runtime.getRuntime().exec("chmod +x " + filePath);

        } catch (IOException e) {
        }
    }
}
