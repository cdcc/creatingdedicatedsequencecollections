/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.hgsrubingh.keggapi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Richard Rubingh
 */
public class OrganismFromGeneNameExtractor {

    static private final String baseurl = "http://rest.kegg.jp/info/";
    ArrayList<String> geneEntries = new ArrayList<>();

    public void getGeneEntriesFromKegg(String ECNumber) throws MalformedURLException, IOException {

        String pattern = "[\\w]{3}";
        // Create the object pattern
        Pattern r = Pattern.compile(pattern);

        // Create matcher object
        Matcher m = r.matcher(ECNumber);

        //check the input for ec number.
        if (m.find()) {

            String filteredECNumber = m.group(0);
            //minimum size of ec number string is 4 to prevent too many results.
            if (true) {
                System.out.println(baseurl + filteredECNumber);
                URL urlRead = new URL(baseurl + filteredECNumber);

                try (BufferedReader input = new BufferedReader(
                        new InputStreamReader(urlRead.openStream()))) {
                    String inputLine;
                    while ((inputLine = input.readLine()) != null) {

                        String[] bits = inputLine.split("\t");

                        System.out.println(bits[0]);

                        geneEntries.add(inputLine);

                    }
                }

            }
        } else {

        }

    }

    @Override
    public String toString() {
        return "GetGeneEntriesFromECNumber{" + "geneEntries=" + geneEntries + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.geneEntries);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrganismFromGeneNameExtractor other = (OrganismFromGeneNameExtractor) obj;
        if (!Objects.equals(this.geneEntries, other.geneEntries)) {
            return false;
        }
        return true;
    }

    public ArrayList<String> getGeneEntries() {
        return geneEntries;
    }

}
