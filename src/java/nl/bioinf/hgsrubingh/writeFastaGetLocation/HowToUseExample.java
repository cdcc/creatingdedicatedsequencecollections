/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.hgsrubingh.writeFastaGetLocation;

import java.io.IOException;

/**
 *
 * @author zertox
 */
public class HowToUseExample {
    public static void main(String[] args) throws IOException {
        
        WriteFastaGetLocation a = new WriteFastaGetLocation();
        a.WriteAndGetFasta("/home/zertox/Desktop/testfasta.fa", ">bay:RBAM_021290 resA; thiol-disulfide oxidoreductase (EC:1.8.4.2) (A)\n" +
"MNKKRRRLFIRTGILLILLCALGYTIYNAVYTNGERISEGSKAPNFALKDTEGRLIELSQ\n" +
"LKGKGVFLNFWGTWCEPCKQEFPYMENQYKHFKDLGVEVVAVNVGESKIAVHNFMKTYGV\n" +
"NFPVALDTDRQVLDAYGVSPIPTTFLINPDGKVVKVVTGTMTERMVHDYMNLIKPEASS"
        );
        
        System.out.println(a.getRonaldsMap());
        
    }
    
}
