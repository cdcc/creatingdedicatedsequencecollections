/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.hgsrubingh.writeFastaGetLocation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author zertox
 */
public class WriteFastaGetLocation {
    Map<String, String> filePathHashMap = new HashMap<>();

    public void WriteAndGetFasta(String filePath, String fastaFile) throws IOException {
        File file = new File(filePath);

        // if file doesnt exists, then create it
        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        try (BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write(fastaFile);
        }
        Process p = Runtime.getRuntime().exec("chmod 777 " + filePath);
        System.out.println("Done");
        
        filePathHashMap.put("pathToFile", filePath);

    }

    public Map<String, String> getRonaldsMap() {
        return filePathHashMap;
    }

}
