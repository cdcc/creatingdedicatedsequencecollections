package nl.bioinf.sboersma.helloform.servlets;
// Import required java libraries
 import nl.bioinf.rnieuwenhuis.controller.IdCreator;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nl.bioinf.hgsrubingh.keggapisimplefied.GetGeneEntriesFromECNumber;
import nl.bioinf.hgsrubingh.keggapisimplefied.GetGeneEntriesFromGeneName;
import nl.bioinf.hgsrubingh.keggapisimplefied.GetGeneEntrySequence;
import nl.bioinf.hgsrubingh.writeFastaGetLocation.WriteFastaGetLocation;
import nl.bioinf.rnieuwenhuis.controller.Controller;

// Extend HttpServlet class
public class HelloFormTest extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession s = request.getSession();
        

        if (request.getParameter("gene_ID") == null){
            Controller.getQuerySequence(request);
            String input = request.getParameter( "inputType" );
            String output = request.getParameter( "outputType" );
            String email = request.getParameter( "email" );
            s.setAttribute("input",input);
            s.setAttribute("output",output);
            s.setAttribute("email",email);
            RequestDispatcher view = request.getRequestDispatcher("ArgumentsTestDynamic.jsp");
            view.forward(request, response); 
        }
        
        else{
            IdCreator idCreator = new IdCreator();
            Integer id = idCreator.readIdFile();
            s.setAttribute("user_id",id);
            GetGeneEntrySequence geneEntry = new GetGeneEntrySequence();
            
            geneEntry.getGeneEntriesFromKegg(request.getParameter("gene_ID"),s.getAttribute("input").toString());
            WriteFastaGetLocation writeFastaGetLocation = new WriteFastaGetLocation();
            String filepath = "/commons/student/2014-2015/Thema10/DedicatedSequenceTool/%s.fa";
            String absoluteFilepath = String.format(filepath, id.toString());
            writeFastaGetLocation.WriteAndGetFasta(absoluteFilepath, geneEntry.getFasta());
            HashMap<String, String> info = (HashMap<String, String>) writeFastaGetLocation.getRonaldsMap();
            info.put("ID", id.toString());
            info.put("evalue", "1");
            info.put("wordSize", "3");
            String input = (String) s.getAttribute("input");
            String output = (String) s.getAttribute("output");
            if( input.equals("AA") && output.equals("AA")){
                info.put("Type", "blastp");              
            } else if (input.equals("NT") && output.equals("NT")){
                info.put("Type", "blastn");
            } else if (input.equals("AA") && output.equals("NT")){
                info.put("Type", "tblastn");
            } else if (input.equals("NT") && output.equals("AA")){
                info.put("Type", "blastx");
            }
            Controller.executeBlast(info, s.getAttribute("email").toString());
            RequestDispatcher view = request.getRequestDispatcher("ArgumentsTestDynamic.jsp");
            view.forward(request, response); 
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
