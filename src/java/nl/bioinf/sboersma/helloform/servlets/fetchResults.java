/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.sboersma.helloform.servlets;

import com.google.gson.Gson;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nl.bioinf.rnieuwenhuis.controller.Controller;
import org.json.JSONObject;

/**
 *
 * @author sboersma
 */
@WebServlet(name = "fetchResults", urlPatterns = {"/fetchResults.do"})
public class fetchResults extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {
            response.setContentType("text/html;charset=UTF-8");
            HttpSession s = request.getSession();
            Controller controller = new Controller();
        if (request.getParameterValues("hiddenTextField") == null) {
            Object filePath = request.getParameter("filePath");
            request.setAttribute("filePath", filePath.toString());
            s.setAttribute("ID",request.getParameter("ID"));
            s.setAttribute("dbType",request.getParameter("dbType"));
            Object json = controller.parseBlast((String) filePath);
            request.setAttribute("jsontest", json);
            
            
            RequestDispatcher view = request.getRequestDispatcher("getParameterFromMail.jsp");
            view.forward(request, response);

        } else {
            FileInputStream in = null;
            String[] geneKeyList = request.getParameterValues("hiddenTextField");           
            String fileName = controller.createDownloadable(geneKeyList,s.getAttribute("ID").toString(),s.getAttribute("dbType").toString());
            Gson gson = new Gson();
            Object json = gson.toJson(geneKeyList);
            request.setAttribute("geneKeys", json);
            
            if(geneKeyList != null){
                response.setContentType("application/force-download");
                OutputStream out = response.getOutputStream();
                in = new FileInputStream(fileName);
                byte[] buffer = new byte[4096];
                int length;
                while ((length = in.read(buffer)) > 0) {
                    out.write(buffer, 0, length);
                }
                in.close();
                out.flush(); 
            }
            
            
//            RequestDispatcher view = request.getRequestDispatcher("getParameterFromMail.jsp");
//            view.forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(fetchResults.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(fetchResults.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
